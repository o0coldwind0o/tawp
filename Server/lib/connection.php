<?php
    $hostname = "127.0.0.1";
    $username = "root";
    $password = "";
    $dbname = "tawp";
    $port = 3306;
    $connection = mysqli_connect($hostname,$username,$password,$dbname,$port);

    if(!$connection) {
        die("Connection Failed: " . mysqli_connect_error());
    }
?>