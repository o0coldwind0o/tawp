<?php
require("../lib/connection.php");
    if (!isset($_SESSION)) {
        session_start();
    }
    $userID =  $_SESSION['id']; // get session about user id
    //sql for get the user id.
    $sql_query_profile_data = "SELECT * FROM `user_profile` Where user_id='$userID'";
    $result_profile = mysqli_query($connection, $sql_query_profile_data);
    
    $row = mysqli_fetch_row($result_profile);
    $name = $row[1];                                //for name if already exists
    $nickname = $row[2];                            //for nickname if already exists
    $day_of_birth = $row[3];                        //for day of birth if already exists
    $gender = $row[4];                              //for gender if already exists
    $contact_email = $row[5];                       //for contact email if already exists
    $telephone = $row[6];                           //for telephone if already exists
    $summary = $row[7];                             //for summary if already exists
    $education_year = $row[8];                      //for education year if already exists
    $education  = $row[9];                          //for education if already exists
    $experience_year  = $row[10];                   //for experience year if already exists
    $experience  = $row[11];                        //for experience if already exists
    $technical_skills  = $row[12];                  //for technical skills if already exists
    $extracurricular_activities_year  = $row[13];   //for extracurricular activities year if already exists
    $extracurricular_activities  = $row[14];        //for extracurricular activities if already exists
    $languages  = $row[15];                         //for languages if already exists
?>


<!DOCTYPE html>
<html lang="en">
    <head>
        <style>
/* table, th, td {
  border: 0 solid black;
} */
div.cv{
    font-size : 150%;
}
        </style>

        <title>Edit Profile</title>
        <link rel="stylesheet" type="text/css" href="../../Client/css/CVdesign.css">
    </head>
    <body>

        <center><font color="#2E3192"size="6">My CV</font><br/><br/></center>

        <table>
            <tr>
                <th colspan = "2">
                    <p align="right"><font color="#2E3192"size="6"><?php echo "$name";?></font></p>
                    <font color="#2E3192"size="4"><p align="right"><?php echo "$contact_email";?></p>
                    <p align="right"><?php echo "$telephone";?></p></font>
                </th>
            </tr>
            <tr>
                <th colspan ="2" align="left"><u>Summary </u></th>
            </tr>
            <tr>
                <th colspan ="2" align="left"><?php echo "$summary";?><br><br></th>
            </tr>
            <tr>
                <th colspan ="2" align="left"><u>Education </u></th>
            </tr>
            <tr>
                <th align="left"><?php echo "$education_year";?></th>
                <th align="left"><?php echo "$education";?><br><br></th>
            </tr>
            <tr>
                <th colspan ="2" align="left"><u>Experience </u></th>
            </tr>
            <tr>
                <th align="left"><?php echo "$experience_year";?></th>
                <th align="left"><?php echo "$experience";?><br><br></th>
            </tr>
            <tr>
                <th colspan ="2" align="left"><u>Technical skills </u></th>
            </tr>
            <tr>
                <th colspan ="2" align="left"><?php echo "$technical_skills";?><br><br></th>
            </tr>
            <tr>
                <th colspan ="2" align="left"><u>Extracurricular activities </u></th>
            </tr>
            <tr>
                <th align="left"><?php echo "$extracurricular_activities_year";?></th>
                <th align="left"><?php echo "$extracurricular_activities";?><br><br></th>
            </tr>
            <tr>
                <th colspan ="2" align="left"><u>Languages </u></th>
            </tr>
            <tr>
                <th colspan ="2" align="left"><?php echo "$languages";?><br><br></th>
            </tr>


            <tr>
                <th colspan ="2">
                    <button class="create" onclick="self.location.href='../../Client/cv.php'">
                        Edit Profile
                    </button>
                </th>
            </tr>

        </table>

    </body>
</html>