<?php
    if (!isset($_SESSION)) {
        session_start();
    }
    if(!isset($_SESSION['id'])) {
        echo "<script>
        {window.alert('Your login has timed out, please log in again!');
        location.href='../index.html'} 
        </script>";
    }
?>