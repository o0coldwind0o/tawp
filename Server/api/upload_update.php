<?php
require("../lib/connection.php");
    if (!isset($_SESSION)) {
        session_start();
    }
    include('login_status_process.php');
    $userID =  $_SESSION['id']; // get session about user id
    $work_id = $_GET['work_id'];

    $sql_query_work_data = "SELECT * FROM `artist_work` Where work_id='$work_id'";

    $result_work_data = mysqli_query($connection, $sql_query_work_data);
    $row = mysqli_fetch_row($result_work_data);
    $work_name = $row[1];                       //for work name if already exists
    $work_description = $row[2];                //for work description if already exists
    $work_type = $row[3];                       //for work type if already exists
    $work_dir = $row[5];                        //for work dir if already exists
    
    if($work_type == 'video'){
        $upload_type = "";
        $button_name = "Update Video";
        $work_result = "<video width='300' height='200' controls>
                       <source src='../../Client/upload_work/$work_dir' type='video/mp4'>
                        </video> ";
        $work_type_select = "<input type='hidden' id='video' name='work_type' value='video'>";
        $button = "<button type='submit' class='create'>$button_name</button>";
    } else if ($work_type == 'audio'){
        $upload_type = "";
        $button_name = "Update Audio";
        $work_result = "<audio controls='controls'>
                        <source src='../../Client/upload_work/$work_dir' type='audio/mpeg'>
                        Your browser does not support the audio element.
                        </audio>";
        $work_type_select = "<input type='hidden' id='audio' name='work_type' value='audio'>";
        $button = "<button type='submit' class='create'>$button_name</button>";
    } else {
        $upload_type = "<td><font color='#2E3192'size='4'>Select image to upload: </font><font color='#FF0000' size='4'>*</font></td>
        <td><input id='myinput' type='file' onchange='encode();'/></td>";
        $button_name = "Update Image";
        $work_result = "<div id='dummy'>$work_dir</div>
        <textarea id='base64' name='img' style='display:none;'></textarea>";
        $work_type_select = "<input type='hidden' id='picture' name='work_type' value='picture'>";
        $button = "<button type='submit' class='create'>$button_name</button>";
    }
 
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Upload work</title>
        <link rel="stylesheet" type="text/css" href="../../Client/css/CVdesign.css">
        <script src="../../Client/js/upload_image.js"></script>
    </head>
    <body>
        <center><font color="#2E3192"size="6">Update Works</font><br/><br/></center>
        <form action="upload_process.php" method="post">
            <table>
                <tr>
                    <td><font color="#2E3192"size="4">Work name: </font><font color="#FF0000" size="4">*</font></td>
                    <td><input type="text" name="workname" placeholder="Work name"
						required="required" maxlength="64" value="<?php echo "$work_name";?>">
                        <?php echo "$work_type_select";?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <font color="#2E3192"size="4"><b>Description: </b></font><br/>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <textarea name="work_description" maxLength="1024" size="15" 
                        placeholder="eg. I am very hard working and organised..." 
                        style="height:100px"><?php echo "$work_description";?></textarea>
                    </td>
                </tr>
                <tr>
                    <?php echo "$upload_type"; ?>
                </tr>
                <tr>
                    <td><font color="#2E3192"size="4">Your work: </font><font color="#FF0000" size="4">*</td>
                    <td colspan="2">
                        <?php echo "$work_result"; ?>
                        <input type="hidden" name="work_id" value="<?php echo "$work_id";?>">
                    </td>
                </tr>
                <tr>
                    <td align='right' colspan='2'><?php echo "$button";?></td>
                </tr>
            </table>
        </form>
    </body>
</html>
