<?php
require("../Server/lib/connection.php");
    if (!isset($_SESSION)) {
        session_start();
    }
    include('../Server/api/login_status_process.php');
    $userID =  $_SESSION['id']; // get session about user id    
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Peer Home Page</title>
        <link rel="stylesheet" type="text/css" href="css/tawp.css">
        <link rel="stylesheet" type="text/css" href="css/home.css">
    </head>
    <body>

        <table style="width:100%; height:100px;">
            <tr>
                <!-- home -->            
                <td colspan="3"><a href="peerHome.php">
                    <img src="images/system_logo_peer.png"></a>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="button" class="headerchoice" style="font-size: 24px; height: 60px;width: 100px;"
                    onclick="location.href='peerHome.php'" value="Home">
                    <a href="search_work.php" target="iframe_a"><button class="headerchoice" style="font-size: 24px; height: 60px;width: 180px;">Search</button></a>
                </td>
                <!-- logout -->
                <td align="right">
                    <input type="button" class="headerchoice" style="font-size: 24px; height: 60px;width: 100px;"
                        onclick="location.href='../Server/api/logout_process.php'" value="logout">
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <nav></nav>
                </td>
            </tr>
        </table>
        <iframe name="iframe_a" src="search_work.php"></iframe>

</body>
</html>