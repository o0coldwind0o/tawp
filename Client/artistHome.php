<?php
require("../Server/lib/connection.php");
    if (!isset($_SESSION)) {
        session_start();
    }
    include('../Server/api/login_status_process.php');
    $userID =  $_SESSION['id']; // get session about user id
    //sql for get the user id.
    $sql_query_profile_data = "SELECT * FROM `user_profile` Where user_id='$userID'";
    $result_profile = mysqli_query($connection, $sql_query_profile_data);
    $button_name = "";
    if (mysqli_num_rows($result_profile) > 0) {
        $button_dir = "../Server/api/cv_process.php";
    } else {
        $button_dir = "cv.php";
    }

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Artist Home Page</title>
        <link rel="stylesheet" type="text/css" href="css/tawp.css">
        <link rel="stylesheet" type="text/css" href="css/home.css">
    </head>
    <body>
        <table style="width:100%; height:100px;">
            <tr>
                <!-- home -->            
                <td colspan="3"><a href="artistHome.php">
                    <img src="images/system_logo_artist.png"></a>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="button" class="headerchoice" style="font-size: 24px; height: 60px;width: 100px;"
                    onclick="location.href='artistHome.php'" value="Home">
                    <a href="upload_video.php" target="iframe_a"><button class="headerchoice" style="font-size: 24px; height: 60px;width: 180px;">Upload Video</button></a>
                    <a href="upload_image.php" target="iframe_a"><button class="headerchoice" style="font-size: 24px; height: 60px;width: 180px;">Upload Image</button></a>
                    <a href="upload_audio.php" target="iframe_a"><button class="headerchoice" style="font-size: 24px; height: 60px;width: 180px;">Upload Audio</button></a>
                    
                </td>
                <!-- logout -->
                <td align="right">
                    <a href="view_mywork.php" target="iframe_a"><button class="headerchoice" style="font-size: 24px; height: 60px;width: 100px;">My Work</button></a>
                    <a href="<?php echo"$button_dir"?>" target="iframe_a"><button class="headerchoice" style="font-size: 24px; height: 60px;width: 100px;">My CV</button></a>
                    <input type="button" class="headerchoice" style="font-size: 24px; height: 60px;width: 100px;"
                        onclick="location.href='../Server/api/logout_process.php'" value="logout">
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <nav></nav>
                </td>
            </tr>
        </table>
        <iframe name="iframe_a" src="view_job.php"></iframe>
</body>
</html>
