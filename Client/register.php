<!DOCTYPE html>
<html lang="en">
	<head>
    	<title>Register Page</title>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="css/login.css">
		<link rel="stylesheet" type="text/css" href="css/check_form.css">
		<script src="js/check_form.js"></script>
	</head>
	<body>
		<header>
			<br/><br/><img src="images/system_logo.png" height="160" width="354">
		</header>
		<nav></nav>
		<div id="login">
			<table>
				<form id="register" method="post" action="../Server/api/register_process.php">
				<br>
					<tr>
						<td colspan="4" style="text-align:center;">Account Application</td>
					</tr>
					<tr>
						<td width="20%"><font color="#2E3192"size="4">Username:</font></td>
						<td colspan="3"><input type="text" name="username" placeholder="Username"
							required="required" maxlength="32" onfocus="showInfo(this);" onblur="checkName(this);">
							<span id="s1" class="show"></span>
						</td>
					</tr>
					<tr>
						<td><font color="#2E3192"size="4">Password:</font></td>
						<td colspan="3"><input type="password" name="password" placeholder="Password"
							required="required" maxlength="32" onblur="checkPwd();" onfocus="showInfo(this);">
							<span id="s2" class="show"></span>	
						</td>
					</tr>
					<tr>
						<td><font color="#2E3192"size="4">Password:</font></td>
						<td colspan="3"><input type="password" name="repassword" placeholder="Password again"
							required="required" maxlength="32" onblur="checkRepwd();">
							<span id="s3" class="show"></span>	
						</td>
					</tr>
					<tr>
						<td><font color="#2E3192"size="4">Email:</font></td>
						<td colspan="3"><input type="email" name="email" placeholder="Email"
							required="required" maxlength="255" onblur="checkEmail();">
							<span id="s6" class="show"></span>	
						</td>
					</tr>
					<tr>
						<td><font color="#2E3192"size="4">Please select your role:</font></td>
						<td>
                        	<input type="radio" id="artist" name="user_type" value="artist" checked="checked">
                        	<label for="artist">Artist</label><br>
                        	<input type="radio" id="employer" name="user_type" value="employer">
                        	<label for="employer">Employer</label><br>
                        	<input type="radio" id="peer" name="user_type" value="peer">
                        	<label for="peer">Peer</label>
						</td>
					</tr>
					<tr>
						<td></td>
						<td><button type="submit" class="login">Apply</button></td>
						<td><button type="reset" class="login">Reset</button></td>
						<td>
							<input class="login" type="button" value="Back" onclick="location.href='../index.html'">
						</td>
					</tr>
				</form>
			</table>
		</div>
	</body>
</html>
