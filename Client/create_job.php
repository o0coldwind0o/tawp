<?php
require("../Server/lib/connection.php");
    if (!isset($_SESSION)) {
        session_start();
    }
    include('../Server/api/login_status_process.php');
$userID =  $_SESSION['id']; // get session about user id


?>

<!DOCTYPE html>
<html>
    <head>
        <title>Create Job</title>
        <link rel="stylesheet" type="text/css" href="css/creator.css">
        <script type="text/javascript" src="jslib/jquery-1.11.1.js"></script>
    </head>
    <body>
    <center><font color="#2E3192"size="6">Create Job Form</font><br/><br/></center>
    <form name="createJob" action="../Server/api/job_create.php" method="POST">
        <table>
            <tr>
                <td colspan="12"><font color="#FF0000" size="4">* is required information </font></td>
            </tr>
            <!-- Job name-->
            <tr>
                <td colspan="3"><font color="#FF0000" size="4">*</font><font color="#2E3192"size="4">Job Name:</font></td></td>
                <td colspan="7"><input type="text" name="jobName" maxLength="64" size="15" required="required"/>
                </td>
            </tr>
            <!-- Job type-->
            <tr>
                <td colspan="3"><font color="#2E3192"size="4"><font color="#FF0000" size="4">*</font>Job Type:</font></td></td>
                <td colspan="7">
                    <div class="styled-select rounded">
                        <select id="jobType" name="jobType">    
                            <option value="Accounting">Accounting</option>
                            <option value="HR">HR</option>
                            <option value="Banking / Heaalth">Banking / Heaalth</option>
                            <option value="Beauty Care / Health">Beauty Care / Health</option>
                            <option value="Building & Construction">Building & Construction</option>
                            <option value="Design">Design</option>
                            <option value="E-commerce">E-commerce</option>
                            <option value="Education">Education</option>
                            <option value="Hospitality / F & B">Hospitality / F & B</option>
                            <option value="IT">Information Technology (IT)</option>
                            <option value="Insurance">Insurance</option>
                            <option value="Management">ManuFacturing</option>
                            <option value="Marketing / Public Relations">Marketing / Public Relations</option>
                            <option value="Media & Advertising">Media & Advertising</option>
                            <option value="Medical Services">Medical Services</option>
                            <option value="Merchandising & Purchasing">Merchandising & Purchasing</option>
                            <option value="Professional Services">Professional Services</option>
                            <option value="Property / Real Estate">Property / Real Estate</option>
                            <option value="Public / Civil">Public / Civil</option>
                            <option value="Sales, CS & Business Devpt">Sales, CS & Business Devpt</option>
                            <option value="Sciences, Lab, R&D">Sciences, Lab, R&D</option>
                            <option value="Transportation & Logistics">Transportation & Logistics</option> 
                        </select>
                    </div>
                </td>
            </tr>
            <!-- region-->
            <tr>
                <td colspan="3"><font color="#FF0000" size="4">*</font><font color="#2E3192"size="4">Location:</font></td></td>
                <td colspan="7">
                    <div class="styled-select rounded">
                        <select id="Location" name="Location">
                            <!-- Hong Kong Island-->
                            <option value="Central & Westerrn">Central & Westerrn</option>
                            <option value="Wanchai">Wanchai</option>
                            <option value="Eastern">Eastern</option>
                            <option value="Southern">Southern</option>
                            <!-- Kowloon-->
                            <option value="Kowloon City">Kowloon City</option>
                            <option value="Wong Tai Sin">Wong Tai Sin</option>
                            <option value="Kwun Tong">Kwun Tong</option>
                            <option value="Yau Tsim Mong">Yau Tsim Mong</option>
                            <option value="Sham Shui Po">Sham Shui Po</option>   
                            <!--New Territories-->
                            <option value="Tsuen Wan">Tsuen Wan</option>
                            <option value="Kwai Tsing">Kwai Tsing</option>
                            <option value="Sai Kung">Sai Kung</option>
                            <option value="Shatin">Shatin</option>
                            <option value="Tai Po">Tai Po</option>
                            <option value="Northern">Northern</option>
                            <option value="Tuen Mun">Tuen Mun</option>
                            <option value="Yuen Long">Yuen Long</option>
                            <option value="Islands">Islands</option>                       
                        </select>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3"><font color="#FF0000" size="4">*</font><font color="#2E3192"size="4">Email:</font></td></td>
                <td colspan="7"><input type="email" name="email" maxLength="255" size="15" required="required"/>
                </td>
			</tr>
            <!-- Career Level -->    
            <tr>
                <td colspan="3"><font color="#FF0000" size="4">*</font><font color="#2E3192"size="4">Career Level:</font></td></td>
                <td colspan="7">
                    <div class="styled-select rounded">
                        <select id="jobLevel" name="jobLevel">
                            <option value="Entry">Entry</option>
                            <option value="Middle">Middle</option>
                            <option value="Senior">Senior</option>
                            <option value="Management">Management</option>
                            <option value="Support">Support</option>
                        </select>
                    </div>
                </td>
            </tr>
            <!-- Work experience -->    
            <tr>
                <td colspan="3"><font color="#2E3192"size="4">&nbsp; Years of Experience:</font></td></td>
                <td colspan="7">       
                    <div class="styled-select rounded">
                        <select id="workExp" name="workExp">
                            <option value="no">N/A</option>
                            <option value="1-2">1-2 years</option>
                            <option value="3-4">3-4 years</option>
                            <option value="above">5 years or above</option>
                        </select>
                    </div>
                </td>
                <td colspan="2" algin="left"></td>
            </tr>
            <!-- Employment Type -->    
            <tr>
                <td colspan="3"><font color="#FF0000" size="4">*</font><font color="#2E3192"size="4">Employment Type:</font></td></td>
                <td colspan="7">
                    <div class="styled-select rounded">
                        <select id="EmploymentType" name="EmploymentType">
                            <option value="Full">Full-Time</option>
                            <option value="Part">Part-Time</option>
                            <option value="Permanent">Permanent</option>
                            <option value="Temporary">Temporary</option>
                            <option value="Contract">Contract</option>
                            <option value="Internship">Internship</option>
                            <option value="Freelance">Freelance</option>
                        </select>
                    </div>
                </td>
            </tr>
            <!-- Salary -->    
            <tr>
                <td colspan="3"><font color="#FF0000" size="4">*</font><font color="#2E3192"size="4">Salary (HKD)</font></td></td>
                <td colspan="7"><input type="text" name="salary" maxLength="16" size="15" required="required" id="salary" placeholder="Salary / Not Specified"/></td>
            </tr>
            <!-- Qualification -->          
            <tr>
                <td colspan="3"><font color="#2E3192"size="4">&nbsp; Qualification:</font></td></td>
                <td colspan="7">
                    <div class="styled-select rounded">
                        <select id="qualification" name="qualification">
                            <option value="N/A">N/A</option>
                            <option value="School Certificate">School Certificate</option>
                            <option value="Non-Degree Tertiary">Non-Degree Tertiary</option>
                            <option value="Matriculated">Matriculated</option>
                            <option value="Degree">Degree</option>
                        </select>
                    </div>
                </td>
                <td colspan="2" algin="left"></td>
            </tr>
            <!-- Start Date -->
            <tr>
                <td colspan="3"><font color="#FF0000" size="4">*</font><font color="#2E3192"size="4">Start Date:</font></td></td>
                <td colspan="7"><input type="datetime-local" name="startDate" required="required" id="startDate" /></td>
            </tr>
            <!-- Deadline -->
            <tr>
                <td colspan="3"><font color="#FF0000" size="4">*</font><font color="#2E3192"size="4">Deadline:</font></td></td>
                <td colspan="7"><input type="datetime-local" name="dueDate" required="required" id="dueDate"/></td>
            </tr>
            <!-- Job Description -->
            <tr>
                <td colspan="12" align="left"><hr/><font color="#2E3192"size="4"><b>Job Description:</b></font><br/><br/></td>
            </tr>
            <tr>
                <td colspan="12" align="left"><font color="#FF0000" size="4">*</font><font color="#2E3192"size="4">Description:</font></td>
            </tr>
            <tr>
                <td colspan="12">
                    <textarea name="description" maxLength="1024" size="15"></textarea>
                    <br/><br/></td>
            </tr>
            <!-- Responsibilities --> 
            <tr>
                <td colspan="12" align="left"><font color="#2E3192"size="4">Responsibilities:</font></td>
            </tr>
            <tr>
                <td colspan="12">
                    <textarea name="responsibilities" maxLength="1024" size="15"></textarea>
                    <br/><br/></td>
            </tr>
            <!-- Requirements --> 
            <tr>
                <td colspan="12" align="left"><font color="#2E3192"size="4">Requirements:</font></td>
            </tr>
            <tr>
                <td colspan="12">
                    <textarea name="requirements" maxLength="1024" size="15"></textarea>
                    <br/><br/></td>
            </tr>
            <!-- Job Function --> 
            <tr>
                <td colspan="12" align="left"><font color="#2E3192"size="4"> Job Functions: </font></td>
            </tr>
            <tr>
                <td colspan="12">
                    <textarea name="jobFunction" maxLength="1024" size="15"></textarea>
                    <br/><br/></td>
            </tr> 
            <!-- Benefits --> 
            <tr>
                <td colspan="12" align="left"><font color="#2E3192"size="4">Benefits: </font></td>
            </tr>
            <tr>
                <td colspan="12">
                    <textarea name="benefits" maxLength="1024" size="15"></textarea>
                    <br/><br/></td>
            </tr> 
            <!-- Others --> 
            <tr>
                <td colspan="12" align="left"><font color="#2E3192"size="4">Others: </font></td>
            </tr>
            <tr>
                <td colspan="12">
                    <textarea name="others" maxLength="1024" size="15"></textarea>
                    <br/><br/></td>
            </tr> 
            <!-- submit -->
            <tr>
                <td colspan="10">
                    <button type="submit" class="create" >Create</button>
                </td>
                <td><button type="reset" class="create">Reset</button>
                </td>
            </tr>               
        </table>
    </form>
</body>
</html>