<?php
require("../Server/lib/connection.php");
    if (!isset($_SESSION)) {
        session_start();
    }
    include('../Server/api/login_status_process.php');
    $userID =  $_SESSION['id']; // get session about user id
    //sql for get the user id.
    $sql_query_profile_data = "SELECT * FROM `user_profile` Where user_id='$userID'";
    $result_profile = mysqli_query($connection, $sql_query_profile_data);
    
    $row = mysqli_fetch_row($result_profile);
    $name = $row[1];                                //for name if already exists
    $nickname = $row[2];                            //for nickname if already exists
    $day_of_birth = $row[3];                        //for day of birth if already exists
    $gender = $row[4];                              //for gender if already exists
    $contact_email = $row[5];                       //for contact email if already exists
    $telephone = $row[6];                           //for telephone if already exists
    $summary = $row[7];                             //for summary if already exists
    $education_year = $row[8];                      //for education year if already exists
    $education  = $row[9];                          //for education if already exists
    $experience_year  = $row[10];                   //for experience year if already exists
    $experience  = $row[11];                        //for experience if already exists
    $technical_skills  = $row[12];                  //for technical skills if already exists
    $extracurricular_activities_year  = $row[13];   //for extracurricular activities year if already exists
    $extracurricular_activities  = $row[14];        //for extracurricular activities if already exists
    $languages  = $row[15];                         //for languages if already exists        
    $button_name = "Create";
    if (mysqli_num_rows($result_profile) > 0) {
        $button_name = "Update";
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Create CV</title>
        <link rel="stylesheet" type="text/css" href="css/CVdesign.css">
        <link rel="stylesheet" type="text/css" href="css/check_form.css">
        <script src="js/check_form.js"></script>
    </head>
    <body>
    <center><font color="#2E3192"size="6"><?php echo "$button_name";?> CV</font><br/><br/></center>
    <form name="createCV" action="../Server/api/cv_create.php" method="POST">
        <table>
            <!-- name -->
            <tr>
                <td colspan="2"><font color="#2E3192"size="4">Name: </font><font color="#FF0000" size="4">*</font></td>
                <td><input type="text" name="name" maxLength="32" size="20" 
                required="required" onfocus="showInfo(this);" onblur="checkInputName(this);"
                placeholder="eg. Chan Tai Man" value="<?php echo "$name";?>"/></td>
                <td colspan="10"><span id="s4" class="show"></span></td>
            </tr>
            <!-- nickname -->
            <tr>
                <td colspan="2"><font color="#2E3192"size="4">Nickname:</td>
                <td><input type="text" name="nickname" maxLength="32" size="20" 
                placeholder="eg. Chan Tai Man" value="<?php echo "$nickname";?>"/></td>
            </tr>
            <!-- day of birth -->
            <tr>
                <td colspan="2"><font color="#2E3192"size="4">Day of birth: </font><font color="#FF0000" size="4">*</font></td>
                <td><input type="date" name="day_of_birth" required="required" 
                onfocus="showInfo(this);" onblur="checkDayOfBirth(this);" value="<?php echo "$day_of_birth";?>"/></td>
                <td colspan="10"><span id="s5" class="show"></span></td>
            </tr>
            <!-- gender -->
            <tr>
                <td colspan="2"><font color="#2E3192"size="4">Gender: </font><font color="#FF0000" size="4">*</font></td>
                <td>
                    <input type="radio" id="male" name="gender" value="m" checked="checked">
                    <label for="male">Male</label>
                    <input type="radio" id="female" name="gender" value="f">
                    <label for="female">Female</label>
                </td>

            </tr>
            <!-- Contact_email -->
            <tr>
                <td colspan="2"><font color="#2E3192"size="4">Contact email : </font><font color="#FF0000" size="4">*</font></td>
                <td><input type="email" name="contact_email" maxLength="255" size="15" 
                onfocus="showInfo(this);" onblur="checkEmail(this);"
                required="required" placeholder="eg. ChanTaiMan@gmail.com"/
                value="<?php echo "$contact_email";?>"></td>
                <td colspan="10"><span id="s6" class="show"></span></td>
            </tr>
            <!-- phone number-->
            <tr>
                <td colspan="2"><font color="#2E3192"size="4">Telephone: </font><font color="#FF0000" size="4">*</font></td>
                <td><input type="number" name="telephone" maxLength="8" size="15"
                onfocus="showInfo(this);" onblur="checkTelephone(this);"
                required="required" placeholder="eg. 23456789" value="<?php echo "$telephone";?>"/></td>
                <td colspan="10"><span id="s7" class="show"></span></td>
            </tr>
            <!-- Summary-->
            <tr>
                <td colspan="12" align="left"><hr/>
                    <font color="#2E3192"size="4"><b>Summary :</b></font><br/>
                </td>
            </tr>
            <tr>
                <td colspan="12">
                    <textarea name="summary" maxLength="1024" size="15" 
                    placeholder="eg. I am very hard working and organised..." 
                    style="height:100px"><?php echo "$summary";?></textarea>
                </td>

            </tr>
            <!-- Education--> 
            <tr>
                <td colspan="12" align="left"><font color="#2E3192"size="4"><b>Education:</b></font></td>
            </tr>
            <tr>
                <td><textarea name="education_year" maxLength="32" size="15" style="height:100px" 
                    placeholder="eg. 2018-2019 SS programmer"><?php echo "$education_year";?></textarea>
                </td>
                <td colspan="12">
                    <textarea name="education" maxLength="1024" size="15" style="height:100px" 
                    placeholder="eg. I have design the SEN system"><?php echo "$education";?></textarea>
                </td>
            </tr>
            <!-- Experience  --> 
            <tr>
                <td colspan="12" align="left"><font color="#2E3192"size="4"><b>Experience:</b></font></td>
            </tr>
            <tr>
                <td><textarea name="experience_year" maxLength="32" size="15" 
                    style="height:100px"><?php echo "$experience_year";?></textarea>
                </td>
                <td colspan="12">
                    <textarea name="experience" maxLength="1024" size="15" 
                    style="height:100px"><?php echo "$experience";?></textarea>
                </td>
            </tr>
            <!-- Technical skills --> 
            <tr>
                <td colspan="12" align="left"><font color="#2E3192"size="4"><b>Technical skills:</b></font></td>
            </tr>
            <tr>
                <td colspan="12">
                    <textarea name="technical_skills" maxLength="1024" size="15" 
                    style="height:100px"><?php echo "$technical_skills";?></textarea>
                </td>
            </tr>
            <!-- Extracurricular activities --> 
            <tr>
                <td colspan="12" align="left"><font color="#2E3192"size="4"><b>Extracurricular activities:</b></font></td>
            </tr>
            <tr>
                <td><textarea name="extracurricular_activities_year" maxLength="16" size="15" 
                style="height:100px"><?php echo "$extracurricular_activities_year";?></textarea>
                </td>
                <td colspan="12">
                    <textarea name="extracurricular_activities" maxLength="1024" size="15" 
                    style="height:100px"><?php echo "$extracurricular_activities";?></textarea>
                </td>
            </tr>
            <!-- Languages --> 
            <tr>
                <td colspan="12" align="left"><font color="#2E3192"size="4"><b>Languages :</b></font></td>
            </tr>
            <tr>
                <td colspan="12">
                    <textarea name="languages" maxLength="1024" size="15" 
                    style="height:100px"><?php echo "$languages";?></textarea>
                </td>
            </tr>            

            <!-- submit -->
            <tr>
                <td colspan="12"><hr/></td>
            </tr>
            <tr>
                <td colspan="2">
                    <button type="submit" class="create" ><?php echo "$button_name";?></button>
                </td>
            </tr> 
            <tr><td colspan="12"><font color="#FF0000" size="2">* the information should required </font></td></tr>                 
        </table>
    </form>
</body>
</html>