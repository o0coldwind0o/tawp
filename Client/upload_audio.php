<!DOCTYPE html>
<html>
    <head>
        <title>Upload audio</title>
        <link rel="stylesheet" type="text/css" href="css/CVdesign.css">
        <script src="js/upload_image.js"></script>
    </head>
    <body>
        <center><font color="#2E3192"size="6">Upload Audio</font><br/><br/></center>
        <form method="post" enctype="multipart/form-data">
            <table>
                <tr>
                    <td><font color="#2E3192"size="4">Work name: </font><font color="#FF0000" size="4">*</font></td>
                    <td><input type="text" name="workname" placeholder="Work name"
						required="required" maxlength="64">
                        <input type="hidden" id="audio" name="work_type" value="audio">
                    </td>
                </tr>
                <tr>
                    <td>
                        <font color="#2E3192"size="4"><b>Description: </b></font><br/>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <textarea name="work_description" maxLength="1024" size="15" 
                        placeholder="eg. I am very hard working and organised..." 
                        style="height:100px"></textarea>
                    </td>
                </tr>
                <tr>
                    <td>
                        <font color="#2E3192"size="4">Select audio to upload: </font>
                        <font color="#FF0000" size="4">*</font> <br>
                        <font color="#2E3192"size="4">(The maximum file size is 128MB.)</font>
                    </td>
                    <td><input type="file" name="fileToUpload"/></td>
                </tr>
                <tr>
                <td align='right' colspan='2'><input type="submit" value="Uplaod Audio" name="upload_audio" class="create"/></td>
                    <!-- <td align='right' colspan='2'><button type="submit" class="create" >Upload Image</button></td> -->
                </tr>
            </table>
        </form>

    </body>
</html>

<?php
    require("../Server/lib/connection.php");
    if (!isset($_SESSION)) {
        session_start();
    }
    include('../Server/api/login_status_process.php');
    $userID =  $_SESSION['id']; // get session about user id


    error_reporting(1);
    extract($_POST);

    $target_dir = "upload_work/";

    $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);

    if($upload_audio){
        $FileType = pathinfo($target_file,PATHINFO_EXTENSION);

        if($FileType == "mp3" || $FileType == "wma" || $FileType == "wav")
        {
            $workname = $_POST['workname'];
            $work_description = $_POST['work_description'];
            $work_type = $_POST['work_type'];
            $audio_path=$_FILES['fileToUpload']['name'];
            
            //sql for insert the work information such as work_name, work_description, work_type, work_dir and user_id.
            $sqlCreateWork = "INSERT INTO `artist_work` (`work_name`, `work_description`
            , `work_type`, `work_dir`, `user_id`) 
            VALUES ('$workname','$work_description','$work_type','$audio_path','$userID')";
            mysqli_query($connection, $sqlCreateWork); 
    
            move_uploaded_file($_FILES["fileToUpload"]["tmp_name"],$target_file);
    
            
            echo "<script>
            {window.alert('uploaded Your audio \"$workname\" has been uploaded successfully!');
            location.href='view_mywork.php'} 
            </script>";
        } else {
            echo "<script>{window.alert('File Format Not Suppoted');} </script>";
        }
    }
?>