<!DOCTYPE html>
<html>
    <head>
        <title>Upload video</title>
        <link rel="stylesheet" type="text/css" href="css/CVdesign.css">
    </head>
    <body>
    <center><font color="#2E3192"size="6">Upload Video</font><br/><br/></center>
        <table>
            <form method="post" enctype="multipart/form-data">
            <tr>
                <td><font color="#2E3192"size="4">Work name: </font><font color="#FF0000" size="4">*</font></td>
                <td><input type="text" name="workname" placeholder="Work name"
					required="required" maxlength="64">
                    <input type="hidden" id="video" name="work_type" value="video">
                </td>
            </tr>
            <tr>
                <td>
                    <font color="#2E3192"size="4"><b>Description: </b></font><br/>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <textarea name="work_description" maxLength="1024" size="15" 
                    placeholder="eg. I am very hard working and organised..." 
                    style="height:100px"></textarea>
                </td>
            </tr>
            <tr>
                <td>
                    <font color="#2E3192"size="4">Select video to upload: </font>
                    <font color="#FF0000" size="4">*</font> <br>
                    <font color="#2E3192"size="4">(The maximum file size is 128MB.)</font>
                </td>
                <td><input type="file" name="fileToUpload"/></td>
            </tr>

            <tr>
                <td colspan="2" align='right' colspan='2'>
                    <input type="submit" value="Uplaod Video" name="upd" class="create"/>
                    	 
                </td>
            </tr>


            </form>
        </table>
    </body>
</html>



<?php
    require("../Server/lib/connection.php");
    if (!isset($_SESSION)) {
        session_start();
    }
    include('../Server/api/login_status_process.php');
    $userID =  $_SESSION['id']; // get session about user id

    error_reporting(1);
    extract($_POST);

    $target_dir = "upload_work/";

    $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);

    if($upd){
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

        if($imageFileType != "mp4" && $imageFileType != "avi" && $imageFileType != "mov" && $imageFileType != "3gp" && $imageFileType != "mpeg")
        {
            echo "<script>{window.alert('File Format Not Suppoted');} </script>";
        } else {
        $workname = $_POST['workname'];
        $work_description = $_POST['work_description'];
        $work_type = $_POST['work_type'];
        $video_path=$_FILES['fileToUpload']['name'];
        
        //sql for insert the work information such as work_name, work_description, work_type, work_dir and user_id.
        $sqlCreateWork = "INSERT INTO `artist_work` (`work_name`, `work_description`
        , `work_type`, `work_dir`, `user_id`) 
        VALUES ('$workname','$work_description','$work_type','$video_path','$userID')";
        mysqli_query($connection, $sqlCreateWork); 

        move_uploaded_file($_FILES["fileToUpload"]["tmp_name"],$target_file);

        
        echo "<script>
        {window.alert('uploaded Your video \"$workname\" has been uploaded successfully!');
        location.href='view_mywork.php'} 
        </script>";

        }

    }

?>