
var checkName = function($) {
    //1.Get user input
    var value = $.value;
    //2.Defining regular expressions
    var reg = /^\s*$/;
    //3.Get span
    var span = document.getElementById('s1');
    span.innerHTML = "";
    if (reg.test(value)) {
        span.innerHTML = 'Username can not be empty.';
        span.className = 'error';
        return false;
    }
    return true;
}
var checkPwd = function() {
    //1.Get password box
    var pwd = document.forms[0]['password'];
    var reg = /^\s*$/;
    var span = document.getElementById('s2');
    span.innerHTML = '';
        if (reg.test(pwd.value)) {
            span.innerHTML = 'password can not be empty.';
            span.className = 'error';
            return false;
        }
    return true;
}
//Duplicate password check
var checkRepwd = function() {
    //1.Get object
    var repwd = document.forms[0]['repassword'];
    //2.Get the first password
    var pwdValue = document.forms[0]['password'].value;
    var span = document.getElementById('s3');
    span.innerHTML = '';
    if (repwd.value != pwdValue) {
        span.innerHTML = 'Inconsistent passwords.';
        span.className = 'error';
        return false;
    }
    span.innerHTML = '';
    span.className = 'info';
    return true;
}

var checkInputName = function($) {
    //1.Get user input
    var value = $.value;
    //2.Defining regular expressions
    var reg = /^\s*$/;
    //3.Get span
    var span = document.getElementById('s4');
    span.innerHTML = "";
    if (reg.test(value)) {
        span.innerHTML = 'Please fill in the Name.';
        span.className = 'error';
        return false;
    }
    return true;
}

var checkDayOfBirth = function($) {
    //1.Get user input
    var value = $.value;
    //2.Defining regular expressions
    var reg = /^\s*$/;
    //3.Get span
    var span = document.getElementById('s5');
    span.innerHTML = "";
    if (reg.test(value)) {
        span.innerHTML = 'Please fill in the Day of birth.';
        span.className = 'error';
        return false;
    }
    return true;
}

var checkEmail = function($) {
    //1.Get user input
    var value = $.value;
    //2.Defining regular expressions
    var reg = /^\s*$/;
    //3.Get span
    var span = document.getElementById('s6');
    span.innerHTML = "";
    if (reg.test(value)) {
        span.innerHTML = 'Please fill in the Email.';
        span.className = 'error';
        return false;
    }
    return true;
}

var checkTelephone = function($) {
    //1.Get user input
    var value = $.value;
    //2.Defining regular expressions
    var reg = /^\s*$/;
    //3.Get span
    var span = document.getElementById('s7');
    span.innerHTML = "";
    if (reg.test(value)) {
        span.innerHTML = 'Please fill in the Telephone.';
        span.className = 'error';
        return false;
    }
    return true;
}

