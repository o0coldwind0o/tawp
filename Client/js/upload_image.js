function encode(){
    // get the image from select box
    var selectedFile = document.getElementById('myinput').files;
    //check user select image or not
    if(selectedFile.length > 0){
        var imageFile = selectedFile[0];
        // using FileReader class which is build-in javascript
        var fileReader = new FileReader();

        // a listener , if there have image input to FileReader
        fileReader.onload = function(fileLoadEvent){
            // change image to base_64 string
            var srcData = fileLoadEvent.target.result;

            // create a <img/> to show image
            var newImage = document.createElement('img');

            // you can directly insert the base_64 string to <img/> tag
            newImage.src = srcData;

            //show image under the <div="dummy">
            document.getElementById('dummy').innerHTML = newImage.outerHTML;

            // show base_64 string in textarea
            document.getElementById('base64').value = document.getElementById('dummy').innerHTML;
        }

        // input file to fileReader.
        fileReader.readAsDataURL(imageFile);
    }
}
