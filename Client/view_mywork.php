<?php
require("../Server/lib/connection.php");
    if (!isset($_SESSION)) {
        session_start();
    }
    error_reporting(1);
    include('../Server/api/login_status_process.php');
    $userID =  $_SESSION['id']; // get session about user id
    //sql for get the artist work.
    $sql_query_artist_work = "SELECT work_id, work_name, work_description, 
    work_type, upload_date, work_dir FROM `artist_work` Where user_id='$userID'";
    $result = $connection->query($sql_query_artist_work); 


    
    echo "<center><font color='#2E3192'size='6'>My Works</font><br/><br/></center>";
    if ($result->num_rows > 0) {
    echo "<table>";
    while ($row = $result->fetch_assoc()) {
        //for checking the work type
        if ($row['work_type'] == 'video'){
            $work_result = "<video width='600' height='400' controls>
                           <source src='upload_work/$row[work_dir]' type='video/mp4'>
                       </video> ";
        } else if($row['work_type'] == 'audio'){
            $work_result = "<audio controls='controls'>
            <source src='upload_work/$row[work_dir]' type='audio/mpeg'>
          Your browser does not support the audio element.
          </audio>";

        } else {
            $work_result = "<div id='dummy'>$row[work_dir]</div>";
        }
        echo"
            <tr>
                <td><font color='#2E3192'size='4'>Work name:</td>
                <td>$row[work_name]</td>
            </tr>
            <tr>
                <td>
                    <font color='#2E3192'size='4'><b>Description: </b></font><br/>
                </td>
            </tr>
            <tr>
                <td colspan='2'>
                    $row[work_description]
                </td>
            </tr>
            <tr>
                <td><font color='#2E3192'size='4'>Work type:</font></td>
                <td>
                    $row[work_type]
                </td>
            </tr>
            <tr>
                <td colspan='2'>
                    $work_result
                </td>
            </tr>
            <tr>
                <td colspan='2'>";
                $work_id = $row[work_id];
                    $sql_query_mark = "SELECT `mark` 
                        FROM `work_comment`
                        WHERE `work_id` = $work_id AND `mark` IS NOT NULL
                        AND `mark` > 0
                        ";
    
                    $result_m = $connection->query($sql_query_mark);
                    $average_score = 0;
                    $count = 0;
                    if ($result_m->num_rows > 0) {
                        // output data of each row
                        while($rows = $result_m->fetch_assoc()) {
                            $count++;
                            $average_score += $rows[mark];
                        }
                        $average_score = $average_score/$count;
                    }
                    if($average_score == 0){
                        echo "There is no rating<br>";
                    } else {
                        echo "Average score: $average_score<br>";
                    }

                    $sql_query_comment = "SELECT `comment`, `created_date`, `username` 
                    FROM `work_comment`, `artist_work`, `user` 
                    WHERE artist_work.work_id = work_comment.work_id 
                    AND work_comment.user_id = user.user_id AND 
                    work_comment.work_id = '$work_id' ORDER BY `created_date` DESC";

                    $result_c = $connection->query($sql_query_comment);

                    if ($result_c->num_rows > 0) {
                       // output data of each row
                      while($row = $result_c->fetch_assoc()) {
                       echo "$row[created_date] $row[username]: $row[comment]<br>";
                    }
                    } else {
                        echo "no comment<br>";
                    }
                    echo "
                </td>
            </tr>
            <tr>
                <td align='right' colspan='2'>
                <form action='../Server/api/upload_update.php' method='get'>
                    <button name = 'work_id' type='submit'
                     class='create' value='$row[work_id]'>Update work</button>
                </form>
                <form action='../Server/api/delete_work.php' method='GET'>
                    <input type='hidden' name='workid' value='$row[work_id]'>
                    <button type='submit' class='create'>Delete</button>
                </form><hr>
                </td>
            </tr>
            ";
    }
    echo "</table>";
    }else{
        echo "<center><font color='#2E3192'size='4'>You have no uploaded works for the time being.</font><br/><br/></center>";
    }
?>


<!DOCTYPE html>
<html>
    <head>
        <title>My work</title>
        <link rel="stylesheet" type="text/css" href="css/CVdesign.css">
        <script src="js/upload_image.js"></script>
    </head>
    <body>
    </body>
</html>