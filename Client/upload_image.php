<?php
    require("../Server/lib/connection.php");
    if (!isset($_SESSION)) {
        session_start();
    }
    include('../Server/api/login_status_process.php');
    $userID =  $_SESSION['id']; // get session about user id




?>

<!DOCTYPE html>
<html>
    <head>
        <title>Upload image</title>
        <link rel="stylesheet" type="text/css" href="css/CVdesign.css">
        <script src="js/upload_image.js"></script>
    </head>
    <body>
        <center><font color="#2E3192"size="6">Upload Image</font><br/><br/></center>
        <form action="../Server/api/upload_process.php" method="post">
            <table>
                <tr>
                    <td><font color="#2E3192"size="4">Work name: </font><font color="#FF0000" size="4">*</font></td>
                    <td><input type="text" name="workname" placeholder="Work name"
						required="required" maxlength="64">
                        <input type="hidden" id="picture" name="work_type" value="picture">
                    </td>
                </tr>
                <tr>
                    <td>
                        <font color="#2E3192"size="4"><b>Description: </b></font><br/>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <textarea name="work_description" maxLength="1024" size="15" 
                        placeholder="eg. I am very hard working and organised..." 
                        style="height:100px"></textarea>
                    </td>
                </tr>
                <tr>
                    <td><font color="#2E3192" size="4">Select image to upload: </font>
                        <font color="#FF0000" size="4">*</font> <br>
                    </td>
                    <td><input id="myinput" type="file" onchange="encode();"/></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div id="dummy"><!--  Show the image here --></div>
                        <textarea id='base64' name="img" style="display:none;"></textarea>
                        <input type="hidden" name="work_id" value="0">
                    </td>
                </tr>
                <tr>
                    <td align='right' colspan='2'><button type="submit" class="create" >Upload Image</button></td>
                </tr>
            </table>
        </form>

    </body>
</html>