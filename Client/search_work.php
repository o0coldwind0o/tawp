<!DOCTYPE html>
<html>
    <head>
        <title>Search Work</title>
        <link rel="stylesheet" type="text/css" href="css/CVdesign.css">
    </head>
    <body>
    <form name="search_work" method="GET">
        <table>
            <tr>
                <td><font color="#2E3192"size="4">Work Keyword: </font></td>
                <td><input type="text" name="keyword" maxLength="32" size="20" 
                    required="required" />
                </td>
                <td>
                    <input type="radio" id="picture" name="work_type" value="picture">
                    <label for="picture">Picture</label><br>
                    <input type="radio" id="video" name="work_type" value="video">
                    <label for="video">Video</label><br>
                    <input type="radio" id="audio" name="work_type" value="audio">
                    <label for="audio">Audio</label>
                    <input type="radio" id="all" name="work_type" value="all" checked="checked">
                    <label for="all">All</label>
                </td>
                <td>
                    <button type="submit" class="create">Search</button>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>

<?php
    require("../Server/lib/connection.php");
    if (!isset($_SESSION)) {
        session_start();
    }
    include('../Server/api/login_status_process.php');
    $userID =  $_SESSION['id']; // get session about user id 
    extract($_GET);
    error_reporting(1);
    if($keyword==null){
    }else{
        if($work_type == "all"){
            $where_sql = "WHERE ";
        } else {
            $where_sql = "WHERE `work_type` = '$work_type' AND ";
        }
    //sql for get the search artist work results.
    $sql_query_search_artist_work = "
    SELECT `work_id`,`work_name`, `work_description`, `work_type`, 
    `upload_date`, `work_dir` FROM `artist_work`
    $where_sql
    (`work_name` LIKE '%$keyword%' OR `work_description` LIKE '%$keyword%')
    ";

    $result = $connection->query($sql_query_search_artist_work); 
    
    echo "<center><font color='#2E3192'size='6'>Search Results</font><br/><br/></center>";
    if ($result->num_rows > 0) {
    echo "<table>";
    while ($row = $result->fetch_assoc()) {
        //for checking the work type
        if ($row['work_type'] == 'video'){
            $work_result = "<video width='600' height='400' controls>
                           <source src='upload_work/$row[work_dir]' type='video/mp4'>
                       </video> ";
        } else if($row['work_type'] == 'audio'){
            $work_result = "<audio controls='controls'>
            <source src='upload_work/$row[work_dir]' type='audio/mpeg'>
          Your browser does not support the audio element.
          </audio>";

        } else {
            $work_result = "<div id='dummy'>$row[work_dir]</div>";
        }
        echo"
            <tr>
                <td><font color='#2E3192'size='4'>Work name:</td>
            </tr>
            <tr>
                <td colspan='2'>$row[work_name]</td>
            </tr>
            <tr>
                <td>
                    <font color='#2E3192'size='4'><b>Description: </b></font><br/>
                </td>
            </tr>
            <tr>
                <td colspan='2'>
                    $row[work_description]
                </td>
            </tr>
            <tr>
                <td><font color='#2E3192'size='4'>Work type:</font></td>
            </tr>
            <tr>
                <td colspan='2'>
                    $row[work_type]
                </td>
            </tr>
            <tr>
                <td colspan='2'>
                    $work_result
                </td>
            </tr>
            <tr>
                <td>
                    <font color='#2E3192'size='4'><b>Description: </b></font><br/>
                </td>
            </tr>
            <tr>
                <td colspan='2'>
                    $row[work_description]
                </td>
            </tr>
            <tr>
                <td>
                    <font color='#2E3192'size='4'><b>Mark: </b></font><br/>
                </td>
            </tr>
            <tr>
                <td colspan='2'>";
                    $work_id_sql = $row[work_id];

                    $sql_query_mark = "SELECT `mark` 
                        FROM `work_comment`
                        WHERE `work_id` = $work_id_sql AND `mark` IS NOT NULL
                        AND `mark` > 0
                        ";
    
                    $result_m = $connection->query($sql_query_mark);
                    $average_score = 0;
                    $count = 0;
                    if ($result_m->num_rows > 0) {
                        // output data of each row
                        while($rows = $result_m->fetch_assoc()) {
                            $count++;
                            $average_score += $rows[mark];
                        }
                        $average_score = $average_score/$count;
                    }
                    if($average_score == 0){
                        echo "There is no rating";
                    } else {
                        echo "$average_score";
                    }
                    
                    echo"
                </td>
            </tr>
            <tr>
                <td>
                    <font color='#2E3192'size='4'><b>Comment: </b></font><br/>
                </td>
            </tr>
            <tr>
                <td colspan='2'>";
            $work_id = $row[work_id];
            //sql for get the search artist work results include username, 
            //created date and comment.
            $sql_query_comment = "SELECT `comment`, `created_date`, `username` 
                FROM `work_comment`, `artist_work`, `user` 
                WHERE artist_work.work_id = work_comment.work_id 
                AND work_comment.user_id = user.user_id AND work_comment.work_id = $work_id
                ORDER BY `created_date` DESC
                ";

            $result_c = $connection->query($sql_query_comment);

            if ($result_c->num_rows > 0) {
                // output data of each row
                while($row = $result_c->fetch_assoc()) {
                    echo "$row[created_date] $row[username]: $row[comment]<br>";
                }
            } else {
                echo "no comment";
            }


            //sql for check the user is marked.
            $sql_query_marked = "SELECT MAX(`mark`) AS mark
                                FROM `work_comment` 
                                WHERE `user_id` = $userID AND `mark` IS NOT NULL
                                AND `work_id` = $work_id";

            $result_m = $connection->query($sql_query_marked);

            if ($result_m->num_rows > 0) {
                // output data of each row
                if(($row = $result_m->fetch_assoc()) && isset($row[mark])) {
                    $mark_code = "You have rated this work as $row[mark]
                    <input type='hidden' name='mark' value='NULL'><br>
                    ";
                } else {
                $mark_code = "
                    <input type='radio' id='1' name='mark' value='1'>
                    <label for='1'>1</label><br>
                    <input type='radio' id='2' name='mark' value='2'>
                    <label for='2'>2</label><br>
                    <input type='radio' id='3' name='mark' value='3' checked='true'>
                    <label for='3'>3</label><br>
                    <input type='radio' id='4' name='mark' value='4'>
                    <label for='4'>4</label><br>
                    <input type='radio' id='5' name='mark' value='5'>
                    <label for='5'>5</label><br>
                ";
                }
            }

            echo"
                </td>
            </tr>
    
                ";
            echo "
                <tr>
                    <td>
                        <form method='post' action='../Server/api/add_comment.php'>
                            <font color='#2E3192'size='4'><b>Leave a comment here</b></font><br/>
                            <input type='text' name='comment' maxLength='32' size='20'
                            required='required' />
                            <input type='hidden' name='work_id' value='$work_id'>
                            <input type='hidden' name='user_id' value='$userID'>
                            <input type='hidden' name='keyword' value='$keyword'>
                            <input type='hidden' name='work_type' value='$work_type'>
                            <font color='#2E3192'size='4'><b>Your score</b></font><br/>
                            
                            $mark_code
                            <button type='submit' class='create'>Send</button>
                        </form>
                    </td>
                </tr>
                <tr>
                    <td colspan='2'>
                        <hr>
                    </td>
                </tr>
            ";
    }
    echo "</table>";
    }else{
        echo "<center><font color='#2E3192'size='4'>No results asked for \"$keyword\".</font><br/><br/></center>";
    }
    }
?>