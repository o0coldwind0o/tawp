<?php
require("../Server/lib/connection.php");
    if (!isset($_SESSION)) {
        session_start();
    }
    include('../Server/api/login_status_process.php');
$userID =  $_SESSION['id']; // get session about user id


?>

<!DOCTYPE html>
<html>
    <head>
        <title>Employer Home Page</title>
        <link rel="stylesheet" type="text/css" href="css/tawp.css">
        <link rel="stylesheet" type="text/css" href="css/home.css">
    </head>
    <body>
        <table style="width:100%; height:100px;">
            <tr>
                <!-- home -->            
                <td colspan="3"><a href="employerHome.php">
                    <img src="images/system_logo_employer.png"></a>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="button" class="headerchoice" style="font-size: 24px; height: 60px;width: 100px;"
                    onclick="location.href='employerHome.php'" value="Home">
                    <a href="create_job.php" target="iframe_a"><button class="headerchoice" style="font-size: 24px; height: 60px;width: 160px;">Create Job</button></a>
                    <a href="employer_search_work.php" target="iframe_a"><button class="headerchoice" style="font-size: 24px; height: 60px;width: 250px;">Search artist work</button></a>
                </td>
                <!-- logout -->
                <td align="right">
                <a href="view_mypost.php" target="iframe_a"><button class="headerchoice" style="font-size: 24px; height: 60px;width: 120px;">My Post</button></a>
                    <input type="button" class="headerchoice" style="font-size: 24px; height: 60px;width: 100px;"
                        onclick="location.href='../Server/api/logout_process.php'" value="logout">
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <nav></nav>
                </td>
            </tr>
        </table>
        <iframe name="iframe_a" src="employer_search_work.php"></iframe>
</body>
</html>